
#include <iostream>



#include <GPUmat.h>

using namespace Eigen;
using namespace GPU;



int main()
{
  Matrix<float,8,8> mat=Matrix<float,8,8>::Random();
  
  GPUmatrix< Matrix<float,8,8> > mat2(mat);

  
  mat2.flush_to_cpu();
  return 0;
}
