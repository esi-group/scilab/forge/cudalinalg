#include <QR.h>
#include <Eigen/Eigen>
#include <iostream>
using namespace Eigen;
using namespace GPU;
using namespace std;
#include <ctime>



typedef  Matrix<complex<double>,Dynamic,Dynamic> MAT;




int main()
{
  MAT A=MAT::Random(512,256);
  //MAT Abis(A);
  //MAT Ater(A);


  /*HH::TriangularFactorType T = HH::BlockHouseholderTriangularFactor(V.triangularView<UnitLower>(),TAU);
  MAT T2=MAT::Zero();
  _larft('F','C',4,4,V.data(),4,TAU.data(),T2.data(),4);
  cout<<(T-T2).norm()<<endl<<endl;


  HH::applyBlockHouseholderOnTheLeft(A,V,TAU);
  float* WORK=new float[16];
  _larfb('L','T','F','C',4,4,4,V.data(),4,T2.data(),4,Abis.data(),4,WORK,4);
  cout<<(Abis-A).norm()<<endl<<endl;*/

  clock_t t1=clock();

  HouseholderBlockedQR<MAT,12> HQR;
  HQR.compute(A);
  clock_t t2=clock();
  HouseholderQR<MAT> tst2(A);
  clock_t t3=clock();
 float T1=t2-t1;
 T1/=CLOCKS_PER_SEC;
 float T2=t3-t2;
 T2/=CLOCKS_PER_SEC;
  cout<<T1<<endl;
  cout<<T2<<endl;
  /*cout<<"Sample Matrix"<<endl;
  cout<<A<<endl<<endl;
  cout<<"Blocked Algorithm result"<<endl;
  cout<<HQR.matrixQR()<<endl<<endl;
  cout<<"Classic Algorithm result"<<endl;
  cout<<tst2.matrixQR()<<endl<<endl;*/
  cout<<(HQR.matrixQR()-tst2.matrixQR()).norm()<<endl;



 return 0; 
}
