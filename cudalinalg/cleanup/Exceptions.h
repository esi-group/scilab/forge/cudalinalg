/*
 * Exceptions.h
 *
 *  Created on: 17 août 2009
 *      Author: vlj
 */

#ifndef EXCEPTIONS_H_
#define EXCEPTIONS_H_

#include <string>
using namespace std;

class algorithmFailure: public std::exception
{
private:
	string funcname;
	int step;
public:
	algorithmFailure();
	algorithmFailure(string,int);
	~algorithmFailure() throw ();
};

class argumentError: public std::exception
{
private:
	string funcname;
	int argnumber;
public:
	argumentError(string,int);
	~argumentError() throw ();
};

class notImplemanted: public std::exception
{
private:
	string funcname;
public:
	notImplemanted(string);
	~notImplemanted() throw ();
};

#endif /* EXCEPTIONS_H_ */
