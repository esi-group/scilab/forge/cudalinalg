#include "cuLapack.h"
#include <cstdlib>


#define cpuA(i,j) A[((i)-1)+LDA*((j)-1)]
#define gpuA(i,j) MIDX(gA,LDA,i,j)
#define T(i,j) cT[((i)-1)+LDT*((j)-1)]
#define Y(i,j) Y[((i)-1)+LDY*((j)-1)]
#define gY(i,j) MIDX(gY,LDY,i,j)
#define TAU(i) TAU[(i)-1]



template<typename T>
void modifiedLAHR2(int N,int K,int NB, T *A,T *gA, int LDA,T* TAU,T* cT,T* gT,int LDT,T* Y,T* gY,int LDY,T &EI)
{
	int iONE=1;

	T minusOne=-1;
	T dONE=1;
	T dZero=0;
	cudaEvent_t transfertok;
	cudaEventCreate(&transfertok);

	if(N<=1)
		return;

	cublasGetMatrix(LDA,N-K+1,sizeof(T),gA,LDA,A,LDA);

	for(int I=1;I<=NB;++I)
	{
		if(I>1)
		{
			int I2=N-K;
			int I3=I-1;

			dgemv_("N",&I2,&I3,&minusOne,&Y(K+1,1),&LDY,
					&cpuA(K+I-1,1),&LDA,&dONE,&cpuA(K+1,I),&iONE);

			I2=I-1;
			dcopy_(&I2,&cpuA(K+1,I),&iONE,&T(1,NB),&iONE);
			dtrmv_("L","T","U",
					&I2,&cpuA(K+1,1),
					&LDA,&T(1,NB),&iONE);

			I2=N-K-I+1;
			I3=I-1;

			dgemv_("T",&I2,&I3,
					&dONE,&cpuA(K+I,1),
					&LDA,&cpuA(K+I,I),&iONE,&dONE,&T(1,NB),&iONE);

			I2=I-1;

			dtrmv_("U","T","N",
					&I2,cT,&LDT,
					&T(1,NB),&iONE);

			I2=N-K-I+1;
			I3=I-1;

			dgemv_("N",&I2,&I3,&minusOne,
					&cpuA(K+I,1),
					&LDA,&T(1,NB),&iONE,&dONE,&cpuA(K+I,I),&iONE);

			I2=I-1;

			dtrmv_("L","N",
					"U",&I2,
					&cpuA(K+1,1),&LDA,&T(1,NB),&iONE);

			daxpy_(&I2,&minusOne,&T(1,NB),&iONE,&cpuA(K+1,I),&iONE);

			cpuA(K+I-1,I-1)=EI;

		}

		int I2=N-K-I+1;
		int I3=K+I+1;

		dlarfg_(&I2,&cpuA(K+I,I),&cpuA(MIN(K+I+1,N),I),&iONE,&TAU(I));
		EI=cpuA(K+I,I);
		cpuA(K+I,I)=1;

		cublasSetVector(N-K-I+1,sizeof(T),&cpuA(K+I,I),1,gpuA(K+I,I),1);
		cublas_gemv('N',N-K,N-K-I+1,1,gpuA(K+1,I+1),LDA,gpuA(K+I,I),1,0,gY(K+1,I),1);
		cublasGetVector(N-K,sizeof(T),gY(K+1,I),1,&Y(K+1,I),1);


		I2=N-K-I+1;
		I3=I-1;
		dgemv_("T",&I2,&I3,
				&dONE,&cpuA(K+I,1),&LDA,
				&cpuA(K+I,I),&iONE,&dZero,&T(1,I),&iONE);

		I2=N-K;
		I3=I-1;
		dgemv_("N",&I2,&I3,&minusOne,
				&Y(K+1,1),&LDY,
				&T(1,I),&iONE,&dONE,&Y(K+1,I),&iONE);
		dscal_(&I2,&TAU(I),&Y(K+1,I),&iONE);

		I2=I-1;
		T D1=-TAU(I);
		dscal_(&I2,&D1,&T(1,I),&iONE);
		I2=I-1;
		dtrmv_("U","N","N",&I2,cT,&LDT,&T(1,I),&iONE);
		T(I,I)=TAU(I);
	}
	cudaMemcpyAsync(gT,cT,LDT*NB*sizeof(T),cudaMemcpyHostToDevice,0);
	cudaEventRecord(transfertok,0);
	cpuA(K+NB,NB)=EI;
	dlacpy_("A",&K,&NB,&cpuA(1,2),&LDA,Y,&LDY);
	dtrmm_("R","L","N","U",&K,&NB,&dONE,&cpuA(K+1,1),&LDA,Y,&LDY);
	EI=cpuA(K+NB,NB);
	cpuA(K+NB,NB)=1;
	cublasSetMatrix(LDA,NB,sizeof(T),A,LDA,gA,LDA);
	cublasSetMatrix(LDY,NB,sizeof(T),Y,LDY,gY,LDY);
	cublasSetMatrix(LDT,NB,sizeof(T),cT,LDT,gT,LDT);
	if(N>K+NB)
	{
		cublas_gemm('N','N',K,NB,N-K-NB,1,gpuA(1,2+NB),LDA,gpuA(K+1+NB,1),LDA,1,gY,LDY);
	}
	while(cudaEventQuery(transfertok)==cudaErrorNotReady);
	cublas_trmm('R','U','N','N',K,NB,1,gT,LDT,gY,LDY);

	return;

}


