/*
 * DGETRS.cpp
 *
 *  Created on: 16 juil. 2009
 *      Author: vlj
 */


#include "misc.h"
#include "LASWP.h"
#define A(i,j) MIDX(A,LDA,i,j)


template<typename T>
void GETRS(char TRANS,int N, int NRHS,T* A,int LDA,int* IPIV,T* B,int LDB, int &INFO)
{

    /* PRELIMINARY CHECK */
    INFO=0;
    bool NOTRAN=(TRANS=='N'||TRANS=='n');
    bool TTRANS=(TRANS=='T'||TRANS=='t');
    bool TRANSC=(TRANS=='C'||TRANS=='c');
    if (!NOTRAN && !TTRANS && !TRANSC)
        INFO=-1;
    if (N<0)
        INFO=-3;
    if (LDA<MAX(1,N))
        INFO=-5;
    if (LDB<MAX(1,N))
        INFO=-8;
    if (INFO!=0)
//		return; TODO : See why it fails...

        /* Return if matrice are "empty" */
        if (N==0||NRHS==0)
            return;

    if (NOTRAN)
    {
        LASWP<T>(NRHS,B,LDB,1,N,IPIV,1);

        cublas_trsm('L','L','N','U',N,NRHS,1.0,A,LDA,B,LDB);
        cublas_trsm('L','U','N','N',N,NRHS,1.0,A,LDA,B,LDB);

    }
    else
    {
        cublas_trsm('L','U','T','N',N,NRHS,1.0,A,LDA,B,LDB);
        cublas_trsm('L','L','T','U',N,NRHS,1.0,A,LDA,B,LDB);

        LASWP<T>(NRHS,B,LDB,1,N,IPIV,-1);
    }

    return;
}
