/*
 * DLARFG.cpp
 *
 *  Created on: 30 juin 2009
 *      Author: vlj
 */

#include "cuLapack.h"

double sign(double val,double sgn)
{
	return (sgn>=0)?val:-val;
}

void DLARFG(int N,double* ALPHA, double* X, int INCX, double &TAU)
{
	double alpha=dblToHost(ALPHA,0);
	if(N<=1)
	{
		TAU=0;
		return;
	}
	double XNORM=cublasDnrm2(N-1,X,INCX);
	if(XNORM==0)
		TAU=0;
	else
	{
		double BETA=-sign(sqrt(alpha * alpha + XNORM*XNORM),alpha);
		TAU=(BETA-alpha)/BETA;
		cublasDscal(N-1,1/(alpha-BETA),X,INCX);
		setSingleValue(ALPHA,0,BETA);
	}
//TODO : improve precision of BETA...
}

