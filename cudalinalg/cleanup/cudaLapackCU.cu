/*
 * cudaLapack.cpp
 *
 *  Created on: 2 juin 2009
 *      Author: vlj
 */

/*
 * WARNING ! THE IPIV VARIABLE BELOW IS NOT THE SAME AS THE ONE IN FORTRAN ORIGINAL CODE !!!!
 * There is an offset of 1 between position and content, (because of offset of 1 in FORTRAN array) ie :
 * [FORTRAN]IPIV[X] <=> [C] IPIV[X-1]-1
 * TODO : Some working but ugly hack through the code, I must clean the thing...
 */

#include "cudaLapackCU.h"
#include "cublas.h"



/* WARNING ! It's assumed in the following that M is the number of rows,
 * and N is the number of columns,
 * and A the matrix on which modification will happen.
 */
#define IDX(i,j) ((i)-1+((j)-1)*LDA)
#define A(i,j) (A+IDX(i,j))


static double* pinnedHost;
static double* pinnedDevice;


void initLapack()
{
	int err2=cudaSetDeviceFlags(8);
	cudaHostAlloc((void**)&pinnedHost,sizeof(double),2);
	int err=cudaHostGetDevicePointer((void**)&pinnedDevice,(void*)(pinnedHost),0);
	return;
}

int* createPinnedArray(int dim)
{
	int* result;
	cudaHostAlloc((void**)&result,dim*sizeof(int),2);
	return result;
}

void freePinnedArray(int* array)
{
	cudaFreeHost((void*)array);
}


__global__
void retrieveValue(double* a,int index,double* pinnedPtr)
{
	pinnedPtr[0]=a[index];
}

double dblToHost(double* a,int index)
{
	dim3 grid(1);
	dim3 threads(1);

	retrieveValue<<<grid,threads>>>(a,index,pinnedDevice);
	cudaThreadSynchronize();
	return *pinnedHost;
}

float dblToHost(float* a,int index)
{
	float retval;
	cublasGetVector(1,sizeof(float),a+index,1,&retval,1);
	return retval;
}


__global__
void setSingleValue_kernel(double* a,int index, double val)
{
	a[index]=val;
}


void setSingleValue(double* a,int index,double val)
{
	dim3 grid(1);
	dim3 threads(1);
	setSingleValue_kernel<<<grid,threads>>>(a,index,val);
}

void setSingleValue(float* a,int index,float val)
{
	cublasSetVector(1,sizeof(float),&val,1,a+index,1);
}


#include "kernels/transform.cu"

void transformU(double* A,int LDA,int N)
{
	dim3 threads(BLOCKSIZE,BLOCKSIZE);
	dim3 grid(LDA/BLOCKSIZE+1,N/BLOCKSIZE+1);
	transform<'U'><<<grid,threads>>>(A,LDA,N);
}

void transformU(float* A,int LDA,int N)
{
	dim3 threads(BLOCKSIZE,BLOCKSIZE);
	dim3 grid(LDA/BLOCKSIZE+1,N/BLOCKSIZE+1);
	transform<'U'><<<grid,threads>>>(A,LDA,N);
}


void transformHRD(double* A,int LDA)
{
	for(unsigned int I=3;I<=LDA;++I)
	{
		for(unsigned int J=1;J<=I-2;++J)
		{
			HTD(A(I,J),0);
		}
	}
}

void transformHRD(float* A,int LDA)
{
	for(unsigned int I=3;I<=LDA;++I)
	{
		for(unsigned int J=1;J<=I-2;++J)
		{
			HTD(A(I,J),0);
		}
	}
}


void transformQHR(double* A,int LDA,int N)
{
	for(unsigned int I=1;I<=LDA;++I)
	{
		for(unsigned int J=I+1;J<=N;++J)
		{
			HTD(A(I,J),0);
		}
	}
}

void transformQHR(float* A,int LDA,int N)
{
	for(unsigned int I=1;I<=LDA;++I)
	{
		for(unsigned int J=I+1;J<=N;++J)
		{
			HTD(A(I,J),0);
		}
	}
}

#include "kernels/nullify.cu"

void nullify(int M,int N,double* A,int LDA,int MINJ,int MAXJ,int MAXI)
{
	GRIDSETUP(M,N)
	nullify<false><<<grid,threads>>>(A,LDA,MINJ,MAXJ,MAXI);
}

void nullify(int M,int N,float* A,int LDA,int MINJ,int MAXJ,int MAXI)
{
	GRIDSETUP(M,N)
	nullify<false><<<grid,threads>>>(A,LDA,MINJ,MAXJ,MAXI);
}
