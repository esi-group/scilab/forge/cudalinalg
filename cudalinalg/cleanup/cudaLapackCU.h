/*
 * cudaLapackCU.h
 *
 *  Created on: 17 août 2009
 *      Author: vlj
 */

#ifndef CUDALAPACKCU_H_
#define CUDALAPACKCU_H_

#include "misc.h"

#define HYBRID

#define GRIDSETUP(i,j) 	dim3 threads(BLOCKSIZE,BLOCKSIZE);\
		dim3 grid((i)/BLOCKSIZE+1,(j)/BLOCKSIZE+1);

/*
 * Nullify every matrix element under the first subdiagonal.
 *
 * Parameters :
 * - (@IN/OUT) A is the matrix.
 * - (@IN) LDA is the leading dimension of A.
 *
 * template<typename T>
 * void transformHRD(T* A,int LDA);
 */
void transformHRD(double* A,int LDA);
void transformHRD(float* A,int LDA);


/*
 * Nullify every matrix element over the first subdiagonal.
 *
 * Parameters :
 * - (@IN/OUT) A is the matrix.
 * - (@IN) LDA is the leading dimension of A.
 * - (@IN) N is the number of columns of A.
 */
void transformQHR(double* A,int LDA,int N);
void transformQHR(float* A,int LDA,int N);



/*
 * Nullify every matrix element under the diagonal.
 *
 * Parameters :
 * - (@IN/OUT) A is the matrix to transform.
 * - (@IN) LDA is the Leading Dimension of the matrix A.
 * - (@IN) N is the numer of columns of A.
 */
void transformU(double *A,int LDA, int N);
void transformU(float *A,int LDA, int N);

/*
 * Return abs(val) if sgn>0, -abs(val) otherwise.
 */
template<typename T>
T sign(T val,T sgn);

/*
 * Called to initialize memory location for setSingleValue and retrieveValue location.
 */
void initLapack();

/* dblToHost return the value stored in the gpuMemory at a specific location.
 * This function is provided to allow host (ie CPU) to retrieve scalar value from an element
 * inside a matrix. It is used for instance to scale columns.
 *
 * Parameters :
 * - (@in) a is a pointer to a location inside a matrix in gpuMemory :
 * For instance, if A is a pointer to a matrix, A+index will return the A[index-1] element of the
 * matrix seen as a 1-D storage unit, according to the column major storage standard.
 * - (@IN) index is the offet of the location from a, ie the value is seeked at (a+index).
 */
double dblToHost(double* a,int index);
float dblToHost(float* a,int index);

/* setSingleValue store val in the gpuMemory at a specific location.
 * This function is provided to allow host (ie CPU) to store scalar
 * inside a matrix on the GPU. It is used for instance to scale columns.
 *
 * Parameters :
 * - (@IN) a is a pointer to a location inside a matrix in gpuMemory.
 * - (@IN) index is the offset of the location from a.
 * - (@IN) val is the value to store.
 */
void setSingleValue(double* a,int index,double val);
void setSingleValue(float* a,int index,float val);

void nullify(int M,int N,double* A,int LDA,int MINJ,int MAXJ,int MAXI);
void nullify(int M,int N,float* A,int LDA,int MINJ,int MAXJ,int MAXI);

#endif /* CUDALAPACKCU_H_ */
