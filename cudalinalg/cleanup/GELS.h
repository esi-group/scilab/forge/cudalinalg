/*
 * DGELS.cpp
 *
 *  Created on: 12 août 2009
 *      Author: vlj
 */


#define WORK(i) (WORK+(i)-1)

void DGELS(char TRANS,int M,int N, int NRHS,double* A,int LDA,double* B,int LDB, double* WORK, int LWORK, int& INFO)
{
	int MN=MIN(M,N);
	bool TRANST=(TRANS=='T'||TRANS=='t');
	bool TRANSN=(TRANS=='N'||TRANS=='n');
	if(!TRANST && !TRANSN)
		throw argumentError("DGELS",1);
	if(M<0)
		throw argumentError("DGELS",2);
	if(N<0)
		throw argumentError("DGELS",3);
	if(NRHS<0)
		throw argumentError("DGELS",4);
	if(LDA<MAX(1,M))
		throw argumentError("DGELS",6);
	if(LDB<MAX(1,MAX(M,N)))
		throw argumentError("DGELS",8);
	if(LWORK<MAX(1,MN+MAX(MN,NRHS)))
		throw argumentError("DGELS",10);

	if(MIN(M,MIN(N,NRHS))==0)
		return;


	if(M>=N)
		DGEQRF(M,N,A,LDA,WORK,WORK(MN+1),LWORK-MN,INFO);
}
