/*
 * DLARF.cu
 *
 *  Created on: 3 juil. 2009
 *      Author: vlj
 */



void DLARF(char SIDE,int M,int N,double* V,int INCV,double TAU, double* C,int LDC,double* WORK)
{
    bool bSIDEL=(SIDE=='L'||SIDE=='l');
    if (bSIDEL)
    {
        if (TAU!=0)
        {
            cublasDgemv('T',M,N,1,C,LDC,V,INCV,0,WORK,1);
            cublasDger(M,N,-TAU,V,INCV,WORK,1,C,LDC);
        }
    }
    else
    {
        if (TAU!=0)
        {
            cublasDgemv('N',M,N,1,C,LDC,V,INCV,0,WORK,1);
            cublasDger(M,N,-TAU,WORK,1,V,INCV,C,LDC);
        }
    }
}


#ifdef EXPERIMENTAL
void DLARF(char SIDE,int M,int N,double* V,int INCV,double TAU, double* C,int LDC,double* WORK)
{
    int I;
    bool APPLYLEFT=(SIDE=='l'||SIDE=='L');
    int LASTV=0;
    int LASTC=0;
    if (TAU!=0)
    {
        if (APPLYLEFT)
            LASTV=M;
        else
            LASTV=N;
        if (INCV>0)
            I=1+(LASTV-1)*INCV;
        else
            I=1;
        while (LASTV>0 && dblToHost(V,I-1)==0)
        {
            --LASTV;
            I-=INCV;
        }
        if (APPLYLEFT)
            LASTC=ILADLC(LASTV,N,C,LDC)+1;//CAREFUL : I had issue JUST because I forgot to set +1...
        else
            LASTC=0;//TODO : get the last non-zero row in C(:,1:lastv)

        if (APPLYLEFT)
        {
            if (LASTV>0)
            {
                cublasDgemv('T',LASTV,LASTC,1.0,C,LDC,V,INCV,0,WORK,1);
                cublasDger(LASTV,LASTC,-TAU,V,INCV,WORK,1,C,LDC);
            }
        }
        else
        {

            cublasDgemv('N',M,N,1.0,C,LDC,V,INCV,0.0,WORK,1);
            cublasDger(M,N,-TAU,WORK,1,V,INCV,C,LDC);

        }
    }

}
#endif
