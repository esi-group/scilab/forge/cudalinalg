/*
 * Exceptions.cpp
 *
 *  Created on: 12 août 2009
 *      Author: vlj
 */

#include "Exceptions.h"

algorithmFailure::algorithmFailure()
{
  step=0;
  funcname="";
}

algorithmFailure::algorithmFailure(string fname,int n):funcname(fname),step(n)
{

}

algorithmFailure::~algorithmFailure() throw ()
{

}

argumentError::argumentError(string fname,int argn):funcname(fname),argnumber(argn)
{

}

argumentError::~argumentError() throw ()
{

}

notImplemanted::notImplemanted(string fname):funcname(fname)
{

}

notImplemanted::~notImplemanted() throw ()
{

}
