
template<bool scalediag>
__global__
void nullify(double* A,int LDA,int MINJ,int MAXJ,int MAXI)
{
	unsigned int idr=threadIdx.x + blockIdx.x * blockDim.x;
	unsigned int idc=threadIdx.y + blockIdx.y * blockDim.y;



	if(idc+1<=MAXJ && idc+1>=MINJ)
	{
		if(idr+1<=MAXI)
		{
			A[idr+LDA*idc]=0;
		}
		if(scalediag)
		{
			A[idc+LDA*idc]=1;
		}
	}
}

template<bool scalediag>
__global__
void nullify(float* A,int LDA,int MINJ,int MAXJ,int MAXI)
{
	unsigned int idr=threadIdx.x + blockIdx.x * blockDim.x;
	unsigned int idc=threadIdx.y + blockIdx.y * blockDim.y;



	if(idc+1<=MAXJ && idc+1>=MINJ)
	{
		if(idr+1<=MAXI)
		{
			A[idr+LDA*idc]=0;
		}
		if(scalediag)
		{
			A[idc+LDA*idc]=1;
		}
	}
}
