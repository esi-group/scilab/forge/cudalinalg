#include <iostream>
#include <vector>
#include <matrix.h>
#include <cstdlib>
#include <cublasBridge.h>
#include <templates/config.h>
using namespace std;

template<typename T>
void step1(matrix_square_algorithm<T>& A,int j,int jb,T* tau)
{
    int INFO;
    T* work=static_cast<T*>(malloc(NB*NB*NB));
    _geqrf(A.get_gpu_part_height(),jb,A.get_content_at(j,j),A.get_ld(),&tau[j-1],work,NB,INFO);
    free(work);
}

template<typename Tp>
void step2(matrix_square_algorithm<Tp>& A,matrix_flushable<Tp>& B,int j,int jb,Tp* tau)
{

    int sz=A.get_gpu_part_height();
    _larft('F','C',sz,jb,A.get_content_at(j,j),A.get_ld(),&tau[j-1],B.get_cpuptr(),B.get_ld());
    B.flush_to_gpu(jb,jb);
}

template<typename Tp>
void step3(matrix_square_algorithm<Tp> & A,matrix_flushable<Tp> &T,int j,int jb)
{
    A.move_gpu_cpu_pivot(-NB,0);
    int sz=A.get_gpu_part_height();
    int MI1=A.get_rows()-j+1;
    int NIIB1=A.get_columns()-j-jb+1;
    int lda=A.get_rows();
    matrix_flushable<Tp> WORKS(lda,jb);

    cublas_makelowerunit(jb,jb,A.get_ld(),A.get_gpuptr_at(j,j));

    cublas_gemm('T','T',jb,sz,jb,1.0f,T.get_gpuptr(),T.get_ld(),A.get_gpuptr_at(j,j),A.get_ld(),0,WORKS.get_gpuptr(),WORKS.get_ld());
    cublas_gemm('N','N',jb,sz-jb,sz,1.0f,WORKS.get_gpuptr(),WORKS.get_ld(),A.get_gpuptr_at(j,j+jb),A.get_ld(),0,WORKS.get_gpuptr(),WORKS.get_ld());
    cublas_gemm('N','N',sz,sz-jb,jb,-1.0f,A.get_gpuptr_at(j,j),A.get_ld(),WORKS.get_gpuptr(),WORKS.get_ld(),1.0f,A.get_gpuptr_at(j,j+jb),A.get_ld());
    A.move_gpu_cpu_pivot_without_sync(NB,0);
}

template<typename T>
pair<T*,T*> QRFact(T* mat,int m,int n)
{
    int IINFO;
    //T* TAU = static_cast<T*>(malloc(m*sizeof(T)));
    T* TAU = new T[m];
    cublasInit();

    matrix_square_algorithm<T> A(mat,m,n);
    matrix_flushable<T> tmp(NB,NB);


    for (int J=1;J<min(m,n);J+=NB)
    {
        int JB=min(min(m,n)-J+1,NB);

        A.move_gpu_cpu_pivot(NB,0);
        step1(A,J,JB,TAU);
        if (J+JB<m)
        {
            step2(A,tmp,J,JB,TAU);

            step3(A,tmp,J,NB);
            A.move_gpu_cpu_pivot(0,NB);
        }
    }

    return pair<T*,T*>(A.unmap_ptr(),TAU);
}
