/*!
 * \file DLASWP.h
 * \author: vlj
 */

#define IPIV2(k) IPIV2[k-1]

template<typename T>
void LASWP(int N,T* A,int LDA,int K1,int K2,int* IPIV2, int INCX)
{
    int IX0,IX,I1,I2,INC;
    if (N==0)
        return;
    if (INCX==0)
        return;
    if (INCX>0)
    {
        IX0=K1;
        I1=K1;
        I2=K2;
        INC=1;
    }
    if (INCX<0)
    {
        IX0=1+(1-K2)*INCX;
        I1=K2;
        I2=K1;
        INC=-1;
    }
    IX=IX0;
    for(int I=I1;I<=I2;I+=INC)
    {
      int IP=IPIV2(IX);
      cublas_swap(N,A+I-1,LDA,A+IP-1,LDA);
      IX+=INCX;
    }
    return;
}
