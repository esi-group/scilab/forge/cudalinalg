/*!
 * \file LUFact.h
 * \Author vlj
 */

#include <cublasBridge.h>
#include <templates/LASWP.h>
#include <cstdlib>
#include <vector>
#include <matrix.h>
#include <iostream>
#define IPIV(i) IPIV[(i)-1]
#include <templates/config.h>
#include <cassert>

using namespace std;

/*!
 * \brief First Step of the algorithm
 * \param jb Width of the tile
 * \param A Matrix
 * \param IPIV Pivot matrix
 * The current matrix is of the form :
 * \f$ \left(\begin{array}{c} *  \\  \begin{array}{cc|c} * & A & B\end{array}\end{array}\right)\f$
 * where the matrix \f$ (A) \f$ resides in main memory, and \f$ (B) \f$ resides in GPU memory.
 * The first step will factorise (A) in a LU manner with the help of row interchanges.
 * Theses row interchange are stored in the IPIV vector.
 */
template<typename T>
void step1(int j,int jb,matrix_square_algorithm<T>& A,int* IPIV)
{
    int IINFO;
    _getrf(A.get_gpu_part_height(),jb,A.get_content_at(j,j),A.get_ld(),IPIV,IINFO);
}

/*!
 * \brief Second Step of the algorithm
 * \param A Matrix
 * \param J1 An index of IPIV
 * \param J2 An index of IPIV
 * \param IPIV Pivot matrix
 * The current matrix is of the form :
 * \f$ \left(\begin{array}{c} *  \\ \begin{array}{c|c|c} A & B & C\end{array}\end{array}\right) \f$
 * where \f$ (B) \f$ is in the LU form as described in the first step, its columns index are between J1 and J2. The second step will impact the row interchanges
 * that occured in \f$ (B) \f$ to \f$ (A) \f$ in main memory and \f$ (C) \f$ in GPU memory.
 */
template<typename T>
void step2(matrix_square_algorithm<T>& A,int J,int JB,int* IPIV)
{

    for (int I=J;I<=J+JB-1;++I)
    {
        IPIV(I) += J-1;
    }
    int N=A.get_columns();
    if(J+JB<=A.get_columns())
    LASWP( N-J-JB+1,
           A.get_gpuptr_at(1,J+JB),
           A.get_ld(),
           J,J+JB-1 ,IPIV, 1 );
    _laswp( J-1,
            A.get_content_at(1,1),
            A.get_ld(),
            J, J+JB-1, IPIV, 1 );

}


template<typename T>
void step3(matrix_square_algorithm<T>& A,int j,int jb)
{
    if (j+jb<=A.get_columns())
    {

        cublas_trsm('L','L','N','U',
                    jb, A.get_gpu_part_width()-jb,1.0f,
                    A.get_gpuptr_at(j,j),A.get_ld(),
                    A.get_gpuptr_at(j,j+jb),A.get_ld());

        cublas_gemm('N','N',A.get_gpu_part_height()-jb,A.get_gpu_part_width()-jb,jb,-1.0f,A.get_gpuptr_at(j+jb,j),A.get_ld(),A.get_gpuptr_at(j,j+jb),A.get_ld(),1.0f,A.get_gpuptr_at(j+jb,j+jb),A.get_ld());

    }
}

/*!
 * \brief LU Factorisation
 * \param rows Number of rows in the matrix
 * \param cols Number of columns in the matrix
 * \param mat Matrix to be factorised
 * \return Result and pivot matrix in a std::pair
 * The result is packaged as in _getrf for memory footprint reasons.
 * Matrix elements must be continuous
 */

template<typename T>
pair<T*,int*> LUFact(T* mat,int m, int n)
{
    int IINFO;
    int* IPIV = static_cast<int*>(malloc(m*sizeof(int)));
    assert(!cublasInit());

    matrix_square_algorithm<T> A(mat,m,n);


    for (int J=1;J<=min(m,n);J+=NB)
    {
        int JB=min(min(m,n)-J+1,NB);

        A.move_gpu_cpu_pivot(NB,0);
        step1(J,JB,A,&IPIV(J));
        step2(A,J,JB,IPIV);

        A.move_gpu_cpu_pivot(-NB,0);
        step3(A,J,JB);
        A.move_gpu_cpu_pivot(NB,NB);
    }


    return pair<T*,int*>(A.unmap_ptr(),IPIV);
}
