#include <set>
using namespace std;

pair<float*,int*> LUFact_float(int rows,int cols,float* mat);
pair<float*,int*> LUFact_float_d(int rows,int cols,float* mat);
pair<double*,int*> LUFact_double(int rows,int cols,double* mat);

pair<float*,float*> QRFact_float(int,int,float*);
pair<float*,float*> QRFact_float_d(int,int,float*);
pair<double*,double*> QRFact_double(int,int,double*);