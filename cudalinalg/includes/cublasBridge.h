/*
 * cublasBridge.h
 *
 *  Created on: 17 août 2009
 *      Author: vlj
 */

#ifndef CUBLASBRIDGE_H_
#define CUBLASBRIDGE_H_

#include <cublas.h>




/*========================LAPACK====================*/



void _getrf(int i1,int i2, double* i3, int i4, int *i5, int& i6);
void _getrf(int i1,int i2, float* i3, int i4, int *i5, int& i6);

void _laswp(int i1,float* i2,int i3,int i4,int i5,int* i6,int i7);
void _laswp(int i1,double* i2,int i3,int i4,int i5,int* i6,int i7);

void _geqrf(int M,int N,double* A,int LDA, double* TAU, double* WORK,int LWORK,int &INFO);
void _geqrf(int M,int N,float* A,int LDA, float* TAU, float* WORK,int LWORK,int &INFO);

void _larft(char direct,char storev,int N,int K,double* V,int LDV,double* tau,double* t,int LDT);
void _larft(char direct,char storev,int N,int K,float* V,int LDV,float* tau,float* t,int LDT);

void _orgqr(int M,int N,int K,double* A,int LDA,double* TAU,double* WORK,int LWORK,int &INFO);
void _orgqr(int M,int N,int K,float* A,int LDA,float* TAU,float* WORK,int LWORK,int &INFO);

void _trsm(char,char,char,char,int,int,float,float*,int,float*,int);
void _trsm(char,char,char,char,int,int,double,double*,int,double*,int);

void _gemm(char c1,char c2,int i1,int i2,int i3,float f1,float* f2,int i4,float* f3,int i5,float f4,float* f5,int i6);
void _gemm(char c1,char c2,int i1,int i2,int i3,double f1,double* f2,int i4,double* f3,int i5,double f4,double* f5,int i6);
/*========================CUBLAS=======================*/

void cublas_gemm (char transa, char transb, int m, int n, int k,
                            double alpha, const double *A, int lda,
                            const double *B, int ldb, double beta, double *C,
                            int ldc);
void cublas_gemm (char transa, char transb, int m, int n, int k,
                            float alpha, const float *A, int lda,
                            const float *B, int ldb, float beta, float *C,
                            int ldc);

void cublas_trsm (char side, char uplo, char transa,
                            char diag, int m, int n, double alpha,
                            const double *A, int lda, double *B,
                            int ldb);
void cublas_trsm (char side, char uplo, char transa,
                            char diag, int m, int n, float alpha,
                            const float *A, int lda, float *B,
                            int ldb);

void cublas_trmm (char side, char uplo, char transa,
                            char diag, int m, int n, double alpha,
                            const double *A, int lda, double *B,
                            int ldb);
void cublas_trmm (char side, char uplo, char transa,
                            char diag, int m, int n, float alpha,
                            const float *A, int lda, float *B,
                            int ldb);

void cublas_copy (int n, const double *x, int incx, double *y,
                            int incy);

void cublas_copy (int n, const float *x, int incx, float *y,
                            int incy);

void cublas_gemv (char trans, int m, int n, double alpha,
                            const double *A, int lda, const double *x,
                            int incx, double beta, double *y, int incy);

void cublas_gemv (char trans, int m, int n, float alpha,
                            const float *A, int lda, const float *x,
                            int incx, float beta, float *y, int incy);

void cublas_axpy (int n, double alpha, const double *x, int incx,
                            double *y, int incy);

void cublas_axpy (int n, float alpha, const float *x, int incx,
                            float *y, int incy);

void cublas_swap(int n, float* x, int incx, float* y, int incy);    
void cublas_swap(int n, double* x, int incx, float* y, int incy);

void cublas_makelowerunit(int m,int n,int lda,float* A);
void cublas_makelowerunit(int m,int n,int lda,double* A);
#endif /* CUBLASBRIDGE_H_ */
