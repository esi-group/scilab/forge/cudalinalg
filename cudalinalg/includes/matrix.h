#ifndef MATRIX_H
#define MATRIX_H

#include <cstdlib>
#include <cuda/cublas.h>
#include <cstring>
#include <iostream>
#include <cassert>

using namespace std;

template<class T>
class matrix
{
protected:
    T* content;
    int columns;
    int rows;
    int ld;
public:
    int get_columns()
    {
        return columns;
    }

    int get_rows()
    {
        return rows;
    }

    matrix(T*,int n,int m):columns ( n ),rows ( m ),ld ( m )
    {
        //content=static_cast<T*> ( malloc ( rows*columns*sizeof ( T ) ) );
	content = new T[rows*columns];
    }
    int get_ld()
    {
        return ld;
    }

    T* get_content_at ( int i,int j )
    {
        return &content[(i-1)+(j-1)*ld];
    }
    T* unmap_ptr()
    {
        T* ret=content;
        T* null_ptr;
        content=NULL;
        return ret;
    }
    ~matrix()
    {
        if (content)
        {
            delete [] content;
        }
    }
    void display()
    {
        for (int j=0;j<rows;j++)
        {
            for (int i=0;i<columns;i++)
            {
                cout<<content[j+ld*i]<<" ";
            }
            cout<<endl;
        }
    }
};

template<class T>
class matrix_square_algorithm:public matrix<T>
{
private:
    T* gpuptr;
    int starting_columns;
    int starting_rows;
public:
    matrix_square_algorithm ( T* mat,int m,int n ) :matrix<T> ( mat,m,n )
    {
        starting_columns=starting_rows=1;
        cublasAlloc ( matrix<T>::rows * matrix<T>::columns,sizeof ( T ), ( void** ) &gpuptr );
        cublasSetMatrix ( matrix<T>::rows,matrix<T>::columns,sizeof ( T ),mat,matrix<T>::ld,gpuptr,matrix<T>::ld );
    }
    T* get_gpuptr_at(int i,int j)
    {
        assert(i>=starting_rows && j>=starting_columns);
        T* ret=gpuptr+ (i-1) +(j-1)*matrix<T>::ld;
        return ret;
    }
    void move_gpu_cpu_pivot (  int i, int j )
    {

        if ( i>0 )
        {
            assert(
                !cublasGetMatrix ( get_gpu_part_height(),i,sizeof ( T ),
                                   get_gpuptr_at(starting_rows,starting_columns),matrix<T>::ld,
                                   matrix<T>::get_content_at ( starting_rows,starting_columns ),matrix<T>::ld )
            );
            starting_columns+=i;
        }
        if (i<0)
        {
            starting_columns+=i;
            assert(
                !cublasSetMatrix(get_gpu_part_height(),-i,sizeof(T),
                                 matrix<T>::get_content_at(starting_rows,starting_columns),matrix<T>::ld,
                                 get_gpuptr_at(starting_rows,starting_columns),matrix<T>::ld)
            );
        }
        if ( j>0 )
        {
            assert(
                !cublasGetMatrix ( j,get_gpu_part_width(),sizeof ( T ),
                                   get_gpuptr_at(starting_rows,starting_columns),matrix<T>::ld,
                                   matrix<T>::get_content_at ( starting_rows,starting_columns ),matrix<T>::ld )
            );
            starting_rows+=j;
        }
        if (j<0)
        {

            starting_columns+=j;
            assert(
                !cublasSetMatrix(-j,get_gpu_part_width(),sizeof(T),
                                 get_gpuptr_at(starting_rows,starting_columns),matrix<T>::ld,
                                 matrix<T>::get_content_at(starting_rows,starting_columns),matrix<T>::ld)
            );
        }
    }

    void move_gpu_cpu_pivot_without_sync (  int i, int j )
    {

        if ( i>0 )
            starting_columns+=i;
        if (i<0)
            starting_columns+=i;
        if ( j>0 )
            starting_rows+=j;
        if (j<0)
            starting_columns+=j;
    }
    int get_gpu_part_height()
    {
        return matrix<T>::rows - starting_rows+1;
    }
    int get_gpu_part_width()
    {
        return matrix<T>::columns - starting_columns+1;
    }
    T* send_cpu_to_gpu ( int i, int j, int width, int height )
    {
        T* ret;
        assert(! cublasAlloc ( width * height,sizeof ( T ), ( void** ) &ret ));
        assert(! cublasSetMatrix ( width,height,sizeof ( T ),matrix<T>::get_content_at( i,j ),matrix<T>::ld,ret,matrix<T>::ld ) );
        return ret;

    }
    ~matrix_square_algorithm()
    {
        cublasFree(gpuptr);
    }

};

template<class T>
class matrix_flushable:public matrix<T>
{
private:
    T* gpuptr;
public:
    matrix_flushable(int m,int n):matrix<T>(NULL,m,n)
    {
        assert(! cublasAlloc ( m * n,sizeof ( T ), ( void** ) &gpuptr ));
    }
    matrix_flushable(T* mat,int m,int n):matrix<T>(mat,m,n)
    {
        assert(! cublasAlloc ( m * n,sizeof ( T ), ( void** ) &gpuptr ));
        assert(! cublasSetMatrix ( matrix<T>::rows,matrix<T>::columns,sizeof ( T ),mat,matrix<T>::ld,gpuptr,matrix<T>::ld ));
    }

    T* get_gpuptr()
    {
        return gpuptr;
    }

    T* get_cpuptr()
    {
        return matrix<T>::content;
    }
    void flush_to_gpu(int m,int n)
    {
        assert(!cublasSetMatrix ( m,n,sizeof ( T ),matrix<T>::content,matrix<T>::ld,gpuptr,matrix<T>::ld ));
    }

    void flush_to_cpu(int m,int n)
    {
        assert(!cublasGetMatrix ( m,n,sizeof ( T ),gpuptr,matrix<T>::ld,matrix<T>::content,matrix<T>::ld ));
    }

    ~matrix_flushable()
    {
        cublasFree(gpuptr);
    }
};

#endif // MATRIX_H
